package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        var in = new Scanner(System.in);
        var str = in.nextLine();
        System.out.println(checkIns(str));
    }


    public static LinkedList<LinkedList<String>> checkIns(String str) {
        LinkedList<LinkedList<String>> dict = new LinkedList<>();

        var array = str.toCharArray();
        if (array.length > 0) {
            var count = 1;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] == array[i + 1]) {
                    count++;
                } else {
                    if (array[i] != ' ') {
                        var list = new LinkedList<String>();
                        list.add("" + array[i]);
                        list.add("" + count);
                        dict.add(list);
                    }
                    count = 1;
                }
            }
            var list = new LinkedList<String>();
            list.add("" + array[array.length - 1]);
            list.add("" + count);
            dict.add(list);
        }
        return dict;
    }
}