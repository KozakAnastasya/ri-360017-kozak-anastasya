import {Component, Input, OnInit} from '@angular/core';
import {Tasks} from './task-list';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.css']
})
export class TaskManagerComponent implements OnInit {
  tasks = Tasks
  selectedTask: Task;
  public AddTask(newName: string, newDate: string): void {
   const names = this.tasks.map(n => n.name);
   if (names.includes(newName)) {
     return;
   }
   const task = new Task();
   task.date = new Date(Date.parse(newDate));
   task.name = newName;
   Date.parse(newDate);
   this.tasks.push(task);
   this.tasks.sort(function (a, b) {
     if (a.date > b.date) {
       return 1;
     }
     if (a.date < b.date) {
       return -1;
     }
     return 0;
   });
  }
  public DeleteTask(task: Task): void {
    this.tasks = this.tasks.filter(h => h !== task);
  }
  constructor() { }
  ngOnInit() {
  }
  onSelect(task: Task): void {
    this.selectedTask = task;
  }

}
export class Task {
  name: string;
  date: Date;
  // get date(): string {
  //   return this.date.getDate() < 10 ? '0' : '') + this.date.getDate() + '.' +
  //   (this.date.getMonth() < 10 ? '0' : '') + (this.date.getMonth() + 1) + '.' +
  //   this.date.getFullYear();
  // }
}
